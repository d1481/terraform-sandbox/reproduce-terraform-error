resource "gitlab_project" "this" {
  name         = var.project_name
  namespace_id = var.project_namespace_id
  description  = "Created by Terraform - https://gitlab.com/d1481/terraform-sandbox/reproduce-terraform-error"

  initialize_with_readme = var.project_initialize_with_readme
  visibility_level       = var.project_visibility_level
  merge_method           = var.project_merge_method
  issues_enabled         = var.project_issues_enabled
  default_branch         = var.project_default_branch_name

  only_allow_merge_if_all_discussions_are_resolved = var.project_only_allow_merge_if_all_discussions_are_resolved
  only_allow_merge_if_pipeline_succeeds            = var.project_only_allow_merge_if_pipeline_succeeds

  remove_source_branch_after_merge = var.project_remove_source_branch_after_merge
  squash_option                    = var.project_squash_option
}

module "error-project" {
  source = "./modules/project"

  project_name         = "error-project"
  project_namespace_id = 53132129
}

