variable "sre_group_id" {
  type    = number
  default = 14430372
}

variable "protected_approval_group_ids_list" {
  type    = list(number)
  default = []
}

variable "tech_lead_approval_users_list" {
  type    = list(string)
  default = []
}

variable "developer_approval_users_list" {
  type    = list(string)
  default = []
}

#------------------------------------------------
# Project path / group id
#------------------------------------------------

# use the two following variables mutually exclusively

variable "project_namespace_id" {
  type        = number
  description = "Project namespace id. A unique number identifying the group (namespace) where repositories live. Mutually exclusive with project_namespace_path. E.g.: "
  default     = -1
}

variable "project_namespace_path" {
  type        = string
  description = "Project namespace path. A path identifying the group (namespace) where repositories live. Mutually exclusive with project_namespace_id"
  default     = ""
}

#------------

variable "project_default_branch_name" {
  type    = string
  default = "develop"
}

variable "project_staging_branch_name" {
  type    = string
  default = "master"
}

variable "project_name" {
  type = string
}

variable "project_squash_option" {
  type        = string
  default     = "default_on"
  description = "Squash commits when merge request. Valid values are never, always, default_on, or default_off. The default value is default_off."
}

variable "project_deploy_target" {
  type        = string
  default     = "eks"
  description = "Deploy target for the chart(s) in created repo. Default: eks"

  validation {
    condition     = var.project_deploy_target == "eks" || var.project_deploy_target == "openshift"
    error_message = "Variable `project_deploy_target` can only be one of: eks, openshift."
  }
}
