terraform {
  required_version = ">=0.14.5"

  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "3.12.0"
    }
  }
}
