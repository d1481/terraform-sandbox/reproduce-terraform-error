# Gitlab as Code - Helm Infra repository definition

This module creates infrastructure repositories for Helm-enabled applications.

Main properties are:
- Multi protected branch & tag for atlantis workflow (develop -> master -> "vX.Y.Z" tag)
- Developer approvals on develop, tech leads + SRE on other
- paxos suggestions welcome
