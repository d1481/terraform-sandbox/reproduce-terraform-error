data "gitlab_group" "this" {
  count     = var.project_namespace_id == -1 ? 1 : 0
  full_path = var.project_namespace_path
}
