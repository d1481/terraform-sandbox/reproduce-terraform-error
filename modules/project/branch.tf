resource "gitlab_branch" "branch" {
  for_each = { for k, v in local.protected_branches_roles_map: k => v if  k != var.project_default_branch_name }

  name    = each.key
  ref     = var.project_default_branch_name
  project = module.terraform_infra_repo.project_id
}

