resource "gitlab_branch_protection" "protected" {
  for_each = local.protected_branches_roles_map

  project            = module.terraform_infra_repo.project_id
  branch             = each.key
  push_access_level  = each.value
  merge_access_level = "developer"
}

resource "gitlab_project_approval_rule" "protected" {
  for_each = gitlab_branch_protection.protected

  project              = module.terraform_infra_repo.project_id
  name                 = each.value.branch
  approvals_required   = 1
  group_ids            = each.value.branch == "develop" ? var.developer_approval_users_list : concat([var.sre_group_id], var.protected_approval_group_ids_list, var.tech_lead_approval_users_list)
  protected_branch_ids = [each.value.branch_protection_id]
}

