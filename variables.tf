variable "project_name" {
  default = "error-project"
  type = string
}

variable "project_visibility_level" {
  type    = string
  default = "private"
}

variable "project_namespace_id" {
  default = "53132129"
  type = string
}

variable "project_merge_method" {
  type    = string
  default = "ff"
}

variable "project_remove_source_branch_after_merge" {
  type    = bool
  default = true
}

variable "project_issues_enabled" {
  type    = bool
  default = false
}

variable "project_only_allow_merge_if_all_discussions_are_resolved" {
  type    = bool
  default = false
}

variable "project_only_allow_merge_if_pipeline_succeeds" {
  type    = bool
  default = true
}

variable "project_initialize_with_readme" {
  type    = bool
  default = false
}

variable "project_default_branch_name" {
  default = "main"
  type    = string
}

variable "project_squash_option" {
  type        = string
  default = "always"
  description = "Squash commits when merge request. Valid values are never, always, default_on, or default_off. The default value is default_off."
}
